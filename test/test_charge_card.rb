require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_ChargeCard < Minitest::Test
  
  def test_charge_card
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")
  
    request = 
    {
      "account" => "4111111111111111",
      "amount" => 1900,
      "expmmyyyy" => "122024",
      "cvv" => "123",
      "accountHolder" => "Ruby Jones",
      "zipCode" => "10001",
    }
  
    puts "CHARGE Credit Card request >>>"
    puts JSON.pretty_generate(request);
  
    response = redpay.ChargeCard(request)
  
    puts "CHARGE Credit Card response >>>" 
    puts JSON.pretty_generate(response);

    assert_equal "A", response['responseCode']
  end

end

=begin
CHARGE Credit Card request >>>
{
  "account": "4111111111111111",
  "amount": 1900,
  "expmmyyyy": "122024",
  "cvv": "123",
  "accountHolder": "Ruby Jones",
  "zipCode": "10001"
}
CHARGE Credit Card response >>>
{
  "responseCode": "A",
  "transactionId": "DEMO.g2dqd92yo8fio5fn",
  "authCode": "PPS378",
  "planId": null,
  "token": "9418594164541111",
  "cardLevel": "Q",
  "cardBrand": "V",
  "cardType": "C",
  "processorCode": "CC",
  "app": "DEMO",
  "account": "9418594164541111",
  "cardHolderName": "Ruby Jones",
  "amount": 1900,
  "timeStamp": "7/11/2020 8:19:13 PM",
  "text": "Approval Approval",
  "ipAddress": "198.54.106.248:55077",
  "avsCode": "Z"
}
=end
require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_Void < Minitest::Test

  def test_void
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")
  
    charge_request = 
    {
      "account" => "4111111111111111",
      "amount" => 1100,
      "expmmyyyy" => "122024",
      "cvv" => "123",
      "accountHolder" => "Ruby Jones",
      "zipCode" => "10001",
    }
  
    puts "CHARGE Credit Card request >>>"
    puts JSON.pretty_generate(charge_request);
  
    charge_response = redpay.ChargeCard(charge_request)
  
    puts "CHARGE Credit Card response >>>" 
    puts JSON.pretty_generate(charge_response);

    assert_equal "A", charge_response['responseCode']
    assert charge_response['transactionId'] != nil

    if charge_response['transactionId'] != nil
      request = 
      {
        "transactionId" => charge_response['transactionId'],
      }
    
      puts "VOID request >>>"
      puts JSON.pretty_generate(request);
    
      response = redpay.Void(request)
    
      puts "VOID response >>>" 
      puts JSON.pretty_generate(response);

      assert_equal "A", response['responseCode']
    end

  end

end

=begin
VOID request >>>
{
  "transactionId": "DEMO.wxzgiroex8tv2g9v"
}
VOID response >>>
{
  "responseCode": "A",
  "transactionId": "DEMO.wxzgiroex8tv2g9v",
  "authCode": null,
  "planId": null,
  "token": null,
  "cardLevel": null,
  "cardBrand": null,
  "cardType": null,
  "processorCode": "CC",
  "app": "DEMO",
  "account": null,
  "cardHolderName": null,
  "amount": 1100,
  "timeStamp": "7/13/2020 12:43:22 AM",
  "text": "Approval Approval",
  "ipAddress": "198.54.106.248:58421",
  "avsCode": null
}
=end

require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_TokenizeCard < Minitest::Test

  def test_tokenize_card
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")

    request = 
    {
      "account" => "4111111111111111",
      "expmmyyyy" => "122024",
      "cvv" => "123",
      "accountHolder" => "Ruby Jones",
      "zipCode" => "10001",
    }

    puts "TOKENIZE Credit Card request >>>"
    puts JSON.pretty_generate(request);

    response = redpay.TokenizeCard(request)

    puts "TOKENIZE Credit Card response >>>" 
    puts JSON.pretty_generate(response);

    assert_equal "A", response['responseCode']
    assert response['token'] != nil
  end

end

=begin
TOKENIZE Credit Card request >>>
{
  "account": "4111111111111111",
  "expmmyyyy": "122024",
  "cvv": "123",
  "accountHolder": "Ruby Jones",
  "zipCode": "10001"
}
TOKENIZE Credit Card response >>>
{
  "responseCode": "A",
  "transactionId": null,
  "authCode": null,
  "planId": null,
  "token": "29167j3hnflaagsj",
  "cardLevel": null,
  "cardBrand": "V",
  "cardType": "C",
  "processorCode": null,
  "app": "DEMO",
  "account": "1111",
  "cardHolderName": "Ruby Jones",
  "amount": 0,
  "timeStamp": "7/11/2020 8:16:46 PM",
  "text": "APPROVED",
  "ipAddress": "198.54.106.248:55041",
  "avsCode": null
}
=end

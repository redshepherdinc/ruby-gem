require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_ChargeACHToken < Minitest::Test

  def test_charge_ach_token
    
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")

    tokenize_request = 
    {
      "account" => "1234567890",
      "routing" => "121122676",
      "accountHolder" => "Ruby Jones",
    }

    puts "TOKENIZE ACH request >>>"
    puts JSON.pretty_generate(tokenize_request);

    tokenize_response = redpay.TokenizeACH(tokenize_request)

    puts "TOKENIZE ACH response >>>" 
    puts JSON.pretty_generate(tokenize_response);

    assert_equal "A", tokenize_response['responseCode']
    assert tokenize_response['token'] != nil

    if tokenize_response['token'] != nil
      request = 
      {
        "token" => tokenize_response['token'],
        "amount" => 1400,
      }
  
      puts "TOKEN CHARGE ACH request >>>"
      puts JSON.pretty_generate(request);
  
      response = redpay.ChargeToken(request)
  
      puts "TOKEN CHARGE ACH response >>>" 
      puts JSON.pretty_generate(response);

      assert_equal "A", response['responseCode']
    end
  end

end

=begin
TOKEN CHARGE ACH request >>>
{
  "token": "gtedu9b5wsa24z8q",
  "amount": 1400
}
TOKEN CHARGE ACH response >>>
{
  "responseCode": "A",
  "transactionId": "DEMO.7fqlooy4bncpqhsq",
  "authCode": "TD70LY",
  "planId": null,
  "token": "9120988649567890",
  "cardLevel": null,
  "cardBrand": null,
  "cardType": null,
  "processorCode": "CC",
  "app": "DEMO",
  "account": "9120988649567890",
  "cardHolderName": null,
  "amount": 1400,
  "timeStamp": "7/11/2020 11:10:09 PM",
  "text": "Success Success",
  "ipAddress": "198.54.106.248:56147",
  "avsCode": "U"
}
=end

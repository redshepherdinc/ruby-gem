require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_ChargeCardToken  < Minitest::Test

  def test_charge_card_token
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")

    tokenize_request = 
    {
      "account" => "4111111111111111",
      "expmmyyyy" => "122024",
      "cvv" => "123",
      "accountHolder" => "Ruby Jones",
      "zipCode" => "10001",
    }

    puts "TOKENIZE Credit Card request >>>"
    puts JSON.pretty_generate(tokenize_request);

    tokenize_response = redpay.TokenizeCard(tokenize_request)

    puts "TOKENIZE Credit Card response >>>" 
    puts JSON.pretty_generate(tokenize_response);

    assert_equal "A", tokenize_response['responseCode']
    assert tokenize_response['token'] != nil

    if tokenize_response['token'] != nil
      request = 
      {
        "token" => tokenize_response['token'],
        "amount" => 1000,
      }
  
      puts "TOKEN CHARGE Card request >>>"
      puts JSON.pretty_generate(request);
  
      response = redpay.ChargeToken(request)
  
      puts "TOKEN CHARGE Card response >>>" 
      puts JSON.pretty_generate(response);

      assert_equal "A", response['responseCode']
    end

  end

end

=begin
TOKEN CHARGE Credit Card request >>>
{
  "token": "29167j3hnflaagsj",
  "amount": 1600
}
TOKEN CHARGE Credit Card response >>>
{
  "responseCode": "A",
  "transactionId": "DEMO.3n8dzuibjygpdsns",
  "authCode": "PPS274",
  "planId": null,
  "token": "9418594164541111",
  "cardLevel": "Q",
  "cardBrand": "V",
  "cardType": "C",
  "processorCode": "CC",
  "app": "DEMO",
  "account": "9418594164541111",
  "cardHolderName": "Ruby Jones",
  "amount": 1600,
  "timeStamp": "7/11/2020 10:15:42 PM",
  "text": "Approval Approval",
  "ipAddress": "198.54.106.248:55568",
  "avsCode": "Z"
}
=end

require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_Get_Transaction < Minitest::Test

  def test_void
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")
  
    get_transaction_request = 
    {
      "transactionId" => "DEMO.x8ywkijxxvtqfxx2"
    }
  
    puts "GET TRANSACTION request >>>"
    puts JSON.pretty_generate(get_transaction_request);
  
    get_transaction_response = redpay.GetTransaction(get_transaction_request)
  
    puts "GET TRANSACTION response >>>" 
    puts JSON.pretty_generate(get_transaction_response);

    assert_equal "A", get_transaction_response['responseCode']
    assert get_transaction_response['transactionId'] != nil

  end

end

=begin
GET TRANSACTION request >>>
{
  "transactionId": "DEMO.x8ywkijxxvtqfxx2"
}
GET TRANSACTION response >>>
{
  "responseCode": "A",
  "transactionId": "DEMO.x8ywkijxxvtqfxx2",
  "authCode": null,
  "planId": null,
  "token": null,
  "cardLevel": null,
  "cardBrand": null,
  "cardType": null,
  "processorCode": "CC",
  "app": "DEMO",
  "account": null,
  "cardHolderName": null,
  "amount": 1100,
  "timeStamp": "7/13/2020 12:43:22 AM",
  "text": "Approval Approval",
  "ipAddress": "198.54.106.248:58421",
  "avsCode": null
}
=end

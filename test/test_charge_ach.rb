require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_ChargeACH < Minitest::Test

  def test_charge_ach
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")
  
    request = 
    {
      "account" => "1234567890",
      "routing" => "121122676",
      "amount" => 1700,
      "accountHolder" => "Ruby Jones"
    }
  
    puts "CHARGE ACH request >>>"
    puts JSON.pretty_generate(request);
  
    response = redpay.ChargeACH(request)
  
    puts "CHARGE ACH response >>>" 
    puts JSON.pretty_generate(response);

    assert_equal "A", response['responseCode']
  end
  
end

=begin
CHARGE ACH request >>>
{
  "account": "1234567890",
  "routing": "121122676",
  "amount": 1700,
  "accountHolder": "Ruby Jones"
}
CHARGE ACH response >>>
{
  "responseCode": "A",
  "transactionId": "DEMO.labjokoun4t52i6k",
  "authCode": "VNR1LY",
  "planId": null,
  "token": "9120988649567890",
  "cardLevel": null,
  "cardBrand": null,
  "cardType": null,
  "processorCode": "CC",
  "app": "DEMO",
  "account": "9120988649567890",
  "cardHolderName": null,
  "amount": 1700,
  "timeStamp": "7/12/2020 10:57:01 PM",
  "text": "Success Success",
  "ipAddress": "198.54.106.248:57890",
  "avsCode": "U"
}
=end
require 'minitest/autorun'

require './lib/redpay'
require 'json'

class Test_TokenizeACH < Minitest::Test

  def test_tokenize_ach
    redpay = RedPay.new("DEMO", 
    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=", 
    "https://redpaystable.azurewebsites.net/")

    request = 
    {
      "account" => "1234567890",
      "routing" => "121122676",
      "accountHolder" => "Ruby Jones",
    }

    puts "TOKENIZE ACH request >>>"
    puts JSON.pretty_generate(request);

    response = redpay.TokenizeACH(request)

    puts "TOKENIZE ACH response >>>" 
    puts JSON.pretty_generate(response);

    assert_equal "A", response['responseCode']
    assert response['token'] != nil
  end
  
end

=begin
TOKENIZE ACH request >>>
{
  "account": "1234567890",
  "routing": "121122676",
  "accountHolder": "Ruby Jones"
}
TOKENIZE ACH response >>>
{
  "responseCode": "A",
  "transactionId": null,
  "authCode": null,
  "planId": null,
  "token": "gtedu9b5wsa24z8q",
  "cardLevel": null,
  "cardBrand": null,
  "cardType": null,
  "processorCode": null,
  "app": "DEMO",
  "account": "7890",
  "cardHolderName": "Ruby Jones",
  "amount": 0,
  "timeStamp": "7/11/2020 10:27:38 PM",
  "text": "APPROVED",
  "ipAddress": "198.54.106.248:55609",
  "avsCode": null
}
=end

Gem::Specification.new do |s|
    s.name        = 'RedPay'
    s.version     = '1.0.8'
    s.date        = '2020-12-28'
    s.summary     = "Redpay Ruby Gem"
    s.description = "Red Shepherd Payments Ruby Gem"
    s.authors     = ["Red Shepherd Inc"]
    s.email       = 'support@redshepherd.com'
    s.files       = [
                      "lib/redpay.rb",
                      "lib/redpay/charge_ach.rb",
                      "lib/redpay/charge_card.rb",
                      "lib/redpay/charge_token.rb",
                      "lib/redpay/tokenize_ach.rb",
                      "lib/redpay/tokenize_card.rb",
                      "lib/redpay/refund.rb",
                      "lib/redpay/void.rb",
                      "lib/redpay/session.rb",
                      "lib/redpay/get_transaction.rb",
                    ]
    s.homepage    = 'https://www.redshepherd.com'
    s.license     = 'MIT'
  end
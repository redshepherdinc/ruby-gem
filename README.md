### To BUILD the gem

gem build redpay.gemspec

### To PUBLISH the gem

gem push <GEM>

### To INSTALL the gem

gem install RedPay-1.0.5.gem

### To run the test

rake test

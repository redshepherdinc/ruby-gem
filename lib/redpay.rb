require './lib/redpay/charge_card'
require './lib/redpay/charge_ach'
require './lib/redpay/charge_token'
require './lib/redpay/tokenize_card'
require './lib/redpay/tokenize_ach'
require './lib/redpay/refund'
require './lib/redpay/void'
require './lib/redpay/gettransaction'

class RedPay
  def initialize(app, key, endpoint)
    @app      = app
    @key      = key
    @endpoint = endpoint
  end

  def ChargeCard(request)
    charge_card = ChargeCard.new(@app, @key, @endpoint)
    return charge_card.Process(request)
  end

  def ChargeACH(request)
    charge_ach = ChargeACH.new(@app, @key, @endpoint)
    return charge_ach.Process(request)
  end

  def TokenizeCard(request)
    tokenize_card = TokenizeCard.new(@app, @key, @endpoint)
    return tokenize_card.Process(request);
  end

  def TokenizeACH(request)
    tokenize_ach = TokenizeACH.new(@app, @key, @endpoint)
    return tokenize_ach.Process(request);
  end

  def ChargeToken(request)
    charge_token = ChargeToken.new(@app, @key, @endpoint)
    return charge_token.Process(request)
  end

  def Refund(request)
    refund = Refund.new(@app, @key, @endpoint)
    return refund.Process(request);
  end

  def Void(request)
    void = Void.new(@app, @key, @endpoint)
    return void.Process(request);
  end

  def GetTransaction(request)
    get_transaction = GetTransaction.new(@app, @key, @endpoint)
    return get_transaction.Process(request);
  end
end
  
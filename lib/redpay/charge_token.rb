require './lib/redpay/session'

require 'json'
require 'ostruct'

class ChargeToken
  
  def initialize(app, key, endpoint)
    @app      = app
    @key      = key
    @endpoint = endpoint
  end

  def Process(request)
    json_request = JSON.parse(request.to_json, object_class: OpenStruct)

    # Create a session with the server
    session = Session.new(@app, @key, @endpoint + "ecard")

    # Contruct charge token packet
    req = 
    {
      "token" => json_request.token,
      "action" => "TA",
      "amount" => json_request.amount,
      "currency" => json_request.currency ||= "USD"
    }

    return session.Send(req)
  end

end
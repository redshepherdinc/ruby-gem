require './lib/redpay/session'

require 'json'
require 'ostruct'

class ChargeCard
  
  def initialize(app, key, endpoint)
    @app      = app
    @key      = key
    @endpoint = endpoint
  end

  def Process(request)
    json_request = JSON.parse(request.to_json, object_class: OpenStruct)

    # Create a session with the server
    session = Session.new(@app, @key, @endpoint + "ecard")

    # Contruct charge card packet
    req = 
    {
      "account" => json_request.account,
      "action" => "A",
      "amount" => json_request.amount,
      "expmmyyyy" => json_request.expmmyyyy,
      "cvv" => json_request.cvv,
      "cardHolderName" => json_request.accountHolder,
      "avsZip" => json_request.zipCode,
    }

    return session.Send(req)
  end

end
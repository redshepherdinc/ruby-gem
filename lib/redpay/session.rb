require 'net/http'
require 'uri'
require 'json'

require 'openssl'
require 'digest'
require 'base64'
require 'securerandom'

class Session
    def initialize(app, key, endpoint)
      @app      = app
      @key      = key
      @endpoint = endpoint

      # Generate and save Random key and iv using AES Cipher
      @cipher = OpenSSL::Cipher::AES.new(128, :CBC)
      @cipher.encrypt
      
      @random_key = @cipher.random_key
      @random_iv = @cipher.random_iv

      # Have AES decipher ready to decrypt response  
      @decipher = OpenSSL::Cipher::AES.new(128, :CBC)
      @decipher.decrypt
      @decipher.key = @random_key
      @decipher.iv = @random_iv

      # Generate random encrypted key using random_key and RSA public key
      @rsa = OpenSSL::PKey::RSA.new(Base64.decode64(@key))
      @encrypted_random_key = Base64.encode64(@rsa.public_encrypt(Base64.encode64(@random_key)))
      # puts "encrypted_random key ->" + @encrypted_random_key

      # Create session request
      data = 
      {
        "rsaPublicKey" => @key,
        "aesKey" => @encrypted_random_key
      }

      # POST Request for sessionId
      begin
        uri = URI(@endpoint)
        header = {'Content-Type': 'text/json'}
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true if uri.scheme == 'https'
        req = Net::HTTP::Post.new(uri.request_uri, header)
        req.body = data.to_json
        
        # Send the request
        res = http.request(req)

        # Parse and set sessionId
        ses = JSON.parse(res.body)
        @sessionId = ses["sessionId"]
        # puts "sessionId ->" + @sessionId
      rescue => e
        @sessionId = "ERROR #{e}"
      end
    end

    def Send(request)
      plaintext_request = request.to_json;
      #puts "plaintext_request -> " + plaintext_request
      encrypted = @cipher.update(plaintext_request) + @cipher.final
      encrypted_request = Base64.encode64(encrypted)
      #puts "encrypted_request -> " + encrypted_request
      
      data = 
      {
        "sessionId" => @sessionId,
        "app" => @app,
        "iv" => Base64.encode64(@random_iv),
        "aesData" => encrypted_request
      }

      #POST Request packet
      begin
        uri = URI(@endpoint)
        header = {'Content-Type': 'text/json'}
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true if uri.scheme == 'https'
        req = Net::HTTP::Post.new(uri.request_uri, header)
        req.body = data.to_json
        
        # Send the request
        res = http.request(req)

        # Get encrypted response
        encrypted_response = JSON.parse(res.body)

        # Decrypt response
        plain_response = @decipher.update(Base64.decode64(encrypted_response["aesData"])) + @decipher.final
        return JSON.parse(plain_response)
      rescue => e
        return "ERROR #{e}"
      end
    end
end
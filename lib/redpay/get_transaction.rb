require './lib/redpay/session'

require 'json'
require 'ostruct'

class GetTransaction

  def initialize(app, key, endpoint)
    @app      = app
    @key      = key
    @endpoint = endpoint
  end

  def Process(request)
    json_request = JSON.parse(request.to_json, object_class: OpenStruct)

    # Create a session with the server
    session = Session.new(@app, @key, @endpoint + "gettransaction")

    # Contruct get transaction packet
    req = 
    {
      "transactionId" => json_request.transactionId
    }

    return session.Send(req)
  end
  
end
require './lib/redpay/session'

require 'json'
require 'ostruct'

class TokenizeACH

  def initialize(app, key, endpoint)
    @app      = app
    @key      = key
    @endpoint = endpoint
  end
  
  def Process(request)
    json_request = JSON.parse(request.to_json, object_class: OpenStruct)

    # Create a session with the server
    session = Session.new(@app, @key, @endpoint + "token")

    # Contruct tokenize ach packet
    req = 
    {
      "account" => json_request.account,
      "routing" => json_request.routing,
      "accountType" => json_request.accountType ||= "C",
      "action" => "T",
      "cardHolderName" => json_request.accountHolder,
      "currency" => json_request.currency ||= "USD",
      "method" => "ACH"
    }

    return session.Send(req)
  end
  
end